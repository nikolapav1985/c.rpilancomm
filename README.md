Local area network communication using Raspberry Pi
---------------------------------------------------

- communicate inside local area network using Raspberry Pi as a relay
- messages sent to Raspberry Pi are relayed to other devices in the network
- communicate via UDP

Test
----

- run main
- connect to Raspberry Pi ip address port 5000/default using some client
- example client UDP terminal on your [phone](https://play.google.com/store/apps/details?id=com.hardcodedjoy.udpterminal&pli=1)
- type some text to relay to other devices in the network

Todo
----

- attach sensors
- broadcast sensor readings
- store sensor readings on remote server

Compile
-------

- cmake ../rpilancomm
- cmake --build .

Compiler used
-------------

- gcc (build-essential apt package)

Alternative compile
-------------------

- [A](https://www.siliceum.com/en/blog/post/cmake_01_cmake-basics/)
- [B/extensive](https://cliutils.gitlab.io/modern-cmake/modern-cmake.pdf)
- [C/extensive](https://lrita.github.io/images/posts/cplusplus/mastering-cmake.pdf)
- [D](https://vnav.mit.edu/labs/lab1/cmake.html)
- [E](https://www.codeintrinsic.com/getting-started-with-cmake-for-c-cpp/)
